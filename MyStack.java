import java.lang.*;

public class MyStack<T> //using a linked list
{
	//Global variables for base pointer/stack pointer/
	//level below, and in case, stack level above
	//Also a stackheight (i.e. listSize)
	//and an output string for the in-fix to post-fix
	private Level<T> baseLevel;
	//private Level<T> levelBelow;
	//private Level<T> levelAbove;
	private Level<T> currentLevel;
	private int stackHeight;
	private String outputString ="";

	//Define Constructor
	public MyStack()
	{
		baseLevel = new Level(null);
		//levelBelow = new Level(null);
		//levelAbove = new Level(null);
		stackHeight = 0;
		currentLevel = baseLevel;
	}

	public String getOutputString()
	{
		return outputString;
	}

	//Define methods
		//T pop()
	public T pop()
	{

			Level<T> popLevel = this.currentLevel;
			//Level<T> levelBelow = this.currentLevel.getLevelBelow();
			T popLevelData = this.currentLevel.getData();
			//Do not output to string any parenthesis
			if(!popLevelData.equals("("))
			{
				//Concatenate the pop'd data to the output string
				this.outputString += popLevelData;
			}
			//this.currentLevel.setLevelBelow(null);

			// if(this.currentLevel.getLevelBelow() != null)
			// {
			// 	Level<T> levelBelow = this.currentLevel.getLevelBelow();
			// 	this.currentLevel = levelBelow;
			// }
			if(this.currentLevel.getLevelBelow() != null)
			{
				this.currentLevel = this.currentLevel.getLevelBelow();
			}
			else
			{
				this.currentLevel = baseLevel;
			}
			//Pop will remove the levelBelow link from the current level 

			//popLevel.setLevelBelow(null);
			//this.currentLevel = currentLevel.getLevelBelow();

			//if level below != null, set current level to level below
			// if(currentLevel.getLevelBelow() != null)
			// {
			// 	Level<T> levelBelow = this.currentLevel.getLevelBelow();
			// 	this.currentLevel.setLevelBelow(null);
			// 	this.currentLevel = levelBelow;
			// }

			
			// Must set the current level to level below.
			
			stackHeight--;

		return popLevelData;

	}//end of pop()

		// push (T data)
	public void push(T data)
	{
		//Create a new level to hold the data
		Level<T> levelToPush = new Level(data);
		//Create a temp var to assing as the current level
		//  **This will be the level below the one we push
		Level<T> levelBeaneath = this.currentLevel;
		T layerBelowData = this.currentLevel.getData();
		//Store the data to push in a variable 
		T dataToPush = levelToPush.getData();


		//When you push an operand, it goes into Output String
		if(dataToPush.equals("a") || dataToPush.equals("b")
			|| dataToPush.equals("c") || dataToPush.equals("d"))
		{
			this.outputString += dataToPush;
		}

		//When you push open parenthesis: push to stack
		if(dataToPush.equals("("))
		{
			//If the stack is empty, set base level node to this.
			if(stackHeight == 0)
			{
				this.baseLevel = levelToPush;
				levelToPush.setLevelBelow(null);
				//current level stays at base level
			}
			else 
			{
				//If the stackheight is > 0, set Level To Push's
				//to level below to "currentlevel" 
				levelToPush.setLevelBelow(currentLevel);
				//Update current level to the recently pushed level
				this.currentLevel = levelToPush;
			}
			stackHeight++;
		}


		//when you push close parenthesis: pop all until 
		if(dataToPush.equals(")"))
		{
			//Do not push to stack!
			//Pop all until open parenthesis
		//**** This needs a do-while or for loop until it reads a... 
			//**.. ccurrentLevel.getData()lose paranthesis. *
				//pop();
				//pop();
			do
			{
				
				pop();
				//layerBelowData = this.currentLevel.getData();
				//layerBelowData = this.currentLevel.getData();
				//this.currentLevel = this.currentLevel.getLevelBelow();
			}while(!this.currentLevel.getData().equals("("));
			//}while(!layerBelowData.equals("("));
				//layerBelowData = this.currentLevel.getData();
					pop();

			//stackHeight = stackHeight - 2;
		}
		// Operators: + & - are equal // * and div(/) are equal
		if(dataToPush.equals("*"))
		{
			//If the stack is empty, set base level node to this.
			if(stackHeight == 0)
			{
				this.baseLevel = levelToPush;
				levelToPush.setLevelBelow(null);
				//current level stays at base level
				//increment stackheight
				stackHeight++;
				return;
			}
			// if(currentLevel.getData() == null)
			// 		currentLevel.setData(dataToPush);
			// Multiply operations highest operator in this example
			if(this.currentLevel.getData().equals("*"))
			{
				levelToPush.setLevelBelow(this.currentLevel.getLevelBelow());
				//Set the Level to Push's Level Below
				//to the current level's level below
				//Pop the Existing operator
				pop();
				//decrease stackheight
				stackHeight--;
				//this.currentLevel = levelToPush;
				//set the current level to the level to pop
				
				//increase stackheight by 1
				stackHeight++;
				this.currentLevel = levelToPush;
			}
			//Add and subtract dont get popped, data just pushed
			if(currentLevel.getData().equals("+") ||
				currentLevel.getData().equals("-"))
			{
				//
				// if(baseLevel.getData() == null)
				// 	baseLevel.setData(dataToPush);
				//**Level to Pushs level below link is the current level**
				levelToPush.setLevelBelow(this.currentLevel);
				//increment the stackheight
				stackHeight++;
				//set the current level to "levelToPush"
				this.currentLevel = levelToPush;
			}
			else
			{
				levelToPush.setLevelBelow(this.currentLevel);
				this.currentLevel = levelToPush;
			}
		}

		if(dataToPush.equals("+") || dataToPush.equals("-"))
		{
			//If the stack is empty, set base level node to this.
			if(stackHeight == 0)
			{
				this.baseLevel = levelToPush;
				levelToPush.setLevelBelow(null);
				//current level stays at base level
				this.currentLevel = levelToPush;
				//Increment stackheight
				stackHeight++;
				return;
			}
			//If existing Operator is +/-, pop EO and push CO ..******
			if(currentLevel.getData().equals("+") || 
				currentLevel.getData().equals("-"))
			{
				//pop current level
				pop();
				levelToPush.setLevelBelow(this.currentLevel);
				this.currentLevel = levelToPush;

				// //set leveltopush's next link as current level's next
				// levelToPush.setLevelBelow(this.currentLevel.getLevelBelow());
				// //pop current level
				// pop();
				// //set current level to level to push
				// this.currentLevel = levelToPush;

			}
			else
			{
				//Otherwise, set levelToPush below link to 
				//current level
				levelToPush.setLevelBelow(currentLevel);
				//Then set the current level to level to push
				this.currentLevel = levelToPush;
			}	
		}//end of if

	}//end of push(T data)
	

	public boolean isEmpty()
	{
		boolean isEmpty;
		if(baseLevel.getData() != null)
		{
			isEmpty = false;
		}
		else isEmpty = true;
		return isEmpty;
	}



public class Level<T>
	{
		private Level<T> levelBelow;
		private Level<T> levelAbove;
		private T data;
		//write methods for implementation

		public Level(T data)
		{
			this.data = data;
		}

		public Level<T> getLevelAbove()
		{
			return this.levelAbove;
		}

		public Level<T> getLevelBelow()
		{
			return this.levelBelow;
		}

		public void setLevelAbove(Level<T> levelAbove)
		{
			this.levelAbove = levelAbove;
		}

		public void setLevelBelow(Level<T> levelBelow)
		{
			this.levelBelow = levelBelow;
		}

		public T getData()
		{
			return this.data;
		}

		public void setData(T data)
		{
			this.data = data;
		}

	}//end of Level

}//end of class file