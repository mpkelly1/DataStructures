import java.util.*;
import java.lang.*;

public class SinglyLinkedList<T> implements Iterable<T>
{
	private Node<T> head;
	private Node<T> tail;
	private Node<T> current;
	private int listSize; 
	
	//Must write iterator() method for interface
	 public Iterator<T> iterator()
	{
		return new SinglyLinkedListIterator();
	}

	public <T>SinglyLinkedList()
	{
		//Constructor, sets head/tail to null
		//Then links head.nextNode to tail.
		//Initiliaze listSize to 0
		head = new Node(null);
		tail = new Node(null);
		current = head;
		head.setNextNode(tail);
		listSize = 0;
	}

	public void add(T element)
	{
		//adding 'element' to end of list
		Node<T> newNode = new Node<T>(element);
		newNode.setNextNode(head);
		head = newNode;
		listSize++;
	}

	public void insertAt(T element, int index)
	{
		// Start at head node, iterate "i" amt of times
		// when i = index, create new Node and add it.
		Node<T> tempNode = new Node(element);
		Node<T> current = head;

		if (index == 0)
		{
			tempNode.setNextNode(head);
			this.head = tempNode;
		}
		else
		{
			for(int i = 1; i < index; i++)
			{
				current = current.getNextNode();
			}
			tempNode.setNextNode(current.getNextNode());
			current.setNextNode(tempNode);
		}
		listSize++;
	}

	public void remove(T element)
	{
		//remove the first occurence of "element" from list
		Node<T> next = head.getNextNode();
		Node<T> priorNode = this.head;
		//Case 1 - element exists at head node
		if(priorNode.getData() == element)
		{
			//List head gets set to its next node
			this.head = priorNode.getNextNode();
			//the first node with element gets null ref to next
			priorNode.setNextNode(null);
			//derease the list size by one.
			listSize--;
		}
		else
		{	

			for(int i = 0; i < this.listSize; i++)
			{ 
				if(next.getData() == element)
				{
					priorNode.setNextNode(next.getNextNode());
					next.setNextNode(null);
					listSize --;
					break;
				}
				else
				{
					priorNode = next;
					next = next.getNextNode();
				}
			}
		}
	}//end of remove() method

	public void clear()
	{
		//Removes all elements of a list
		//Step one: Start at head, create temp var for 1st node
		//		then create a var for it's next node
		Node<T> current = this.head;
		Node<T> nextNodeInList = current.getNextNode();
		//Two: iterate through listSize, removing the current
		//	node's reference, setting current to nextNode in List.
		for(int i = 0; i < listSize; i++)
		{	
			this.head.setNextNode(null);//set the head's nextNode ref to null
			current = nextNodeInList;//set current node to it's next
			if(i < listSize - 1)
			{
				//Boundary so that the nextNode avoids being
				//set to a non-existent node (i.e. out of list index)
				nextNodeInList = current.getNextNode();
			}
		}
		//reset listSize to 0 after for-loop
		listSize = 0;
	}//end of clear method

	//Method returns TRUE if list is empty
	public boolean isEmpty()
	{
		boolean isEmpty;
		//if there is no data in the head node, return true
		if(this.head.getData() == null)
		{
			isEmpty = true;
		}
		else//if head node has data, then return false
		{
			isEmpty = false;
		}
		return isEmpty;
	}//end of isEmpty()


	//Method returns the number of elements in the list
	public int size()
	{	
		//return the current listSize.
		return this.listSize;
	}

	//Method returns the value "n-th" elements from the first node
	//Count starts with zero
	public T getNthFromFirst(int n)
	{
		//'n' must be < listSize!!! 
		//Create a temp variable for current node, start at head
		//Traverse each node with i <= n
		//if i=n, then return the data of that node
		Node<T> currentNode = this.head;
		T currentNodeData = null;
		if(n < listSize  && n >= 0)
		{
			for(int i = 0; i <= n; i++)
			{
				if(i == n)
				{
					currentNodeData = currentNode.getData();
				}
			}
		}
		else
		{
			System.out.printf("Please re-enter a valid traversal amount.\n",n+1);
		}

		return currentNodeData;
	}//end of getNthFromFirst()

	public T getNthFromLast(int n)
	{
		//'n' must be < listSize && n > 0
		Node<T> currentNode = this.head;
		T currentNodeData = null;
		//Temp Var for the currentNode start @ head
		//Account for case where n=0, return tail data
		if(n == 0)
		{
			currentNodeData = this.tail.getData();
		}
		//Traverse the nodes with i < listSize - (n+1)
		if(n < listSize && n > 0)
		{
			for(int i = 0; i <= listSize - (n+1); i++)
			{
				if(i == n)
				{
					currentNodeData = currentNode.getData();
				}
			}
		}
		else
		{
			System.out.printf("Please enter a valid traversal amount.\n");
		}
		//when i = listSize - (n-1), return data of that node
		return currentNodeData;
	}//end of getNthFromLast()

	public String toString()
	{
		String theReturnString = null;
		Node<T> currentNode = this.head;
		//We want to print out each element of the list.
		for(int i = 0; i < listSize - 1; i++)
		{
			theReturnString += "\n" + currentNode.getData();
		}
		return theReturnString;
	}//end of toString()




	public class SinglyLinkedListIterator implements Iterator<T>
	{
		private Node<T> nextNode;

		public SinglyLinkedListIterator()
		{
			nextNode = head;
		}
		// 	Write methods for implementation
		// hasNext(), next(), and remove() must be implemented
		// **look for exception handling in instructions for remove
		public boolean hasNext()
		{
			//for (<E> data :  )
			return true;
		}

		public T next()
		{
			if(!hasNext()) throw new NoSuchElementException();
			T nextNodeData = nextNode.getData();
			nextNode = nextNode.getNextNode();
			return nextNodeData;
		}//end of next()method

		public void remove()
		{
			throw new UnsupportedOperationException("This operation is not supported.");
		}

	}//end of SinglyLinkedListIterator class



	public class Node<T>
	{
		private Node<T> nextNode;
		private T data;
		//write methods for implementation

		public Node(T data)
		{
			this.data = data;
		}

		public Node getNextNode()
		{
			return this.nextNode;
		}

		public void setNextNode(Node<T> nextNode)
		{
			this.nextNode = nextNode;
		}

		public T getData()
		{
			return this.data;
		}

		public void setData(T data)
		{
			this.data = data;
		}

	}//end of Node


}//end of SinglyLinkedList class