import java.lang.*;

public class StackRunner
{
	public static void main(String[] args)
	{

		MyStack theStack = new MyStack();
		String a = "a";
		String plus = "+";
		String b = "b";
		String multiply = "*";
		String openParen = "(";
		String c = "c";
		String sub = "-";
		String d = "d";
		String closeParen = ")";

		theStack.push(a);
		theStack.push(plus);
		theStack.push(b);
		theStack.push(multiply);
		theStack.push(openParen);
		theStack.push(c);
		theStack.push(sub);
		theStack.push(d);
		theStack.push(closeParen);
		theStack.pop();
		theStack.pop();

		

		System.out.printf("%s\n",theStack.getOutputString());




	}//end of main

}//end of stack runner class file