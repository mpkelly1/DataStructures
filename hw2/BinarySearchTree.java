/*
 * Author: Michael Kelly
 * Homework #2
 * Binary Search Tree
 * Last Update: November 6, 2016
 */
package homework;

public class BinarySearchTree<T extends Comparable<? super T>> extends BinaryTree<T>
{

	//Initialize an empty Binary Search Tree
	public BinarySearchTree()
	{
		super();
	}

	// Init a Binary Search Tree
	//Takes a sequence from a param of Binary Tree
	public BinarySearchTree(T[] seq)
	{
		super(seq);
	}

	//Init Binary Search Tree taking a sequence parm
	// and takes param of a null symbol from Binary Tree
	public BinarySearchTree(T[] seq, T nullSymbol)
	{
		super(seq, nullSymbol);
	} 

	//find Minimum value element 
	// returns null if empty
	public T findMin()
	{
		return findMin(root).getData();
	}

	// find Maximum value element in BST
	// return null if empty
	public T findMax()
	{
		return findMax(root).getData();
	}

	//Insert an element into the tree
	//ignores duplicate values
	//takes a parameter of type T element
	public void insert(T element)
	{
		root = insert(element, root);
	}

	// Mechanics of the insertion in subtrees
	// param 1: the element of type T to insert
	// param 2: the root of the subtree
	// returns the new root of subtree
	private BinaryNode<T> insert(T element, BinaryNode<T> rootNode)
	{
		BinaryNode<T> left = rootNode.getLeftNode();
		BinaryNode<T> right = rootNode.getRightNode();

		int compareValue = element.compareTo(rootNode.getData());

		if (left == null || right == null)
		{
			return rootNode;
		}
		if (rootNode == null)
		{
			insert(element, rootNode);
		}

		if (compareValue < 0 )
		{
			left = insert(element, left);
		}
		else if(compareValue > 0)
		{
			right = insert(element, right);
		}
		else 	; //In case of a duplicate value
					//do nothing
		return rootNode;

	}//end of insert that returns BinaryNode<T>



	public void remove(T element)
	{
		root = remove(element, root);
	}


	//Methods to remove from the subtree
	// parameter 1: element to remove 
	// parameter 2: the root of subtree
	//returns new root of subtree
	private BinaryNode<T> remove(T element, BinaryNode<T> rootNode)
	{
		BinaryNode<T> left = rootNode.getLeftNode();
		BinaryNode<T> right = rootNode.getRightNode();

		int compareValue = element.compareTo(rootNode.getData());

		if(left == null || right == null)
		{
			return rootNode;
		}

		if(compareValue < 0)
		{
			left = remove(element, left);
		}
		else if(left != null && right != null)
		{
			rootNode = findMin(rootNode.getRightNode());
			rootNode = remove(element, right);
		}
		else
		{
			// new root will be left if its not null, 
			// otherwise it will be right node
			rootNode = (left != null ) ? left : right;
		}

		return rootNode;

	}//end of remove that returns BinaryNode<T>


	// the findMin method for smallest element in subtree
	// parameter: rootNode for the subtree
	// returns the node of smallest element

	private BinaryNode<T> findMin(BinaryNode<T> rootNode)
	{
		// Return null if there is no root node 
		if(rootNode == null)
		{
			return null;
		}
		// if the left node is null, root node is the smallest
		else if(rootNode.getLeftNode() == null)
		{
			return rootNode;
		}

		//Recursive call to move further down subtree.
		return findMin(rootNode.getLeftNode());
	}

	// the findMax method for largest element in a subtree
	// parameter: teh root node of the subtree
	// returns the node containing the largest element
	private BinaryNode<T> findMax(BinaryNode<T> rootNode)
	{
		// Keep traversing the BST to the right for the max node
		// returns null if empty
		if(rootNode != null)
		{
			while(rootNode.getRightNode() != null)
			{
				rootNode = rootNode.getRightNode();
			}
		}
		return rootNode;
	}

	// returns a boolean value if an element exists in BST
	public boolean contains(T element)
	{
		return contains(element, root);
	}

	//The contains method to find an item in a subtree
	// Parameter 1: the element to find
	// Parameter 2: rootNode for the subtree to search
	// returns boolean if a match is found
	private boolean contains(T element, BinaryNode<T> rootNode)
	{
		BinaryNode<T> left = rootNode.getLeftNode();
		BinaryNode<T> right = rootNode.getRightNode();

		int compareValue = element.compareTo(rootNode.getData());

		if(left == null || right == null)
		{
			return false;
		}

		//recursive calls to method if element val is..
		// .. less than or greater than the root
		if(compareValue < 0 )
		{
			return contains(element, left);
		}
		else if(compareValue > 0)
		{
			return contains(element, right);
		}
		else
		{
			return true;
		}
	}

}// end of BinarySearchTree class