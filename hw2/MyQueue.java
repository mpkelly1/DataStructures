// Author: Michael Kelly 
// CSCI 2125
// Homework 2: Binary Tree/BST/Queue
// Last Update:  November 6, 2016
package homework;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyQueue<T> implements Iterable<T> 
{

	private T[] queue;
	private int first;
	private int last;
	private int numberOfElements;


	//Constructor
	public MyQueue()
	{
		queue = (T[]) new Object[1];
		numberOfElements = 0;
		first = 0;
		last = 0;
	}

	//Returns true if queue is empty
	//returns false if elements exist
	public boolean isEmpty()
	{
		if(numberOfElements == 0)
			return true;
		else
			return false;
	}

	//Returns the number of elements in the queue
	public int size()
	{
		return numberOfElements;
	}


	//Adds an element of type T to the queue
	//Will resize if necessary
	public void enqueue(T element)
	{
		//If more size is needed, it will double the array
		//then, first element moved to first of new array
		if (numberOfElements == queue.length - 1)
		{
			resize(2 * queue.length);
		}
		queue[last++] = element;

		if (last == queue.length)
		{
			last = 0;
		}
		numberOfElements++;

	}//end of enqueue


	//Remove/return the last element at the end of the queue
	//resize the queue if the space is no longer necessary
	//If the queue is empty, NoSuchElementException thrown
	public T dequeue()
	{
		if (isEmpty())
		{
			throw new NoSuchElementException();
		}
		else
		{
			T returnElement = queue[first];
			queue[first++] = null;
			numberOfElements--;
			if (first == queue.length)
			{
				first = 0;
			}
			if (numberOfElements == queue.length / 4)
			{
				resize(queue.length / 2);
			}

			return returnElement;
		}
	}//end of dequeue()


	//Resizes the array for the queue
	//Use protected declaration for package access
	//Takes a paramater of integer for the size of new array
	protected void resize(int max)
	{
		T[] newQueue = ((T[]) new Object[max]);
		for (int i = 0; i < numberOfElements; i++)
		{
			newQueue[i] = queue[(first + i) % queue.length];
		}
		queue = newQueue;
		first = 0;
		last = numberOfElements;
	}

	//Iterator implementation
	//Returns an iterator for the queue
	@Override
	public Iterator<T> iterator()
	{
		return new MyQueueIterator();
	}

	//Iterator class
	private class MyQueueIterator implements Iterator<T>
	{
		private int index;
		//Constructor
		public MyQueueIterator()
		{
			this.index = 0;
		}

		//hasNext() returns true if the element
		// .. has an element 
		@Override
		public boolean hasNext()
		{
			return index == numberOfElements;
		}

		@Override
		public T next()
		{
			if (!hasNext())
			{
				throw new NoSuchElementException();
			}
			T element = queue [(this.index + first) % queue.length];
			this.index++;
			return element;
		}

	}//end of MyQueueIterator

}//end of class MyQueue

