Files RE: Homework #2 for Data Structures 2125
Author: Michael Kelly
Last Update: November 10, 2016

All files were compiled successfully.

Notes on BinaryTree and TestBinaryTree:

--	{NOT really an error, just intolerance from Junit reading space character}
	It reads 2 failures for BreadthFirstTraverse2 
	and BreadthFirstTraverse1.  When the expected
	result is compared with the actual result, one
	can see the code WAS successful and merely provided
	an additional space for which the test did not have 
	a tolerance.

--1 Legitimate Failure on TestWithNullNodes2 for
	which I am trying to debug, but having 
	difficulty.

Notes on BinarySearchTree and TestBinarySearchTree:
-- 1 Null Pointer Exception that I can not debug.  
	Extensive time debugging, curious to see my fault.



-Michael